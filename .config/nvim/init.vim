" Welcome to my terrible nvim config!

lua require('plugins')
lua require('nvim-lines')

" leader key because I use a 104 keys keyboard
let mapleader = ";"

" set relative numbers
set number relativenumber

" tab size config
set tabstop=4

" indent marking
let g:indentLine_char_list = ['|', '¦', '┆', '┊']

" coc config {
" Use <Ctrl-F> to format documents with prettier
command! -nargs=0 Prettier :CocCommand prettier.formatFile
noremap <C-F> :Prettier<CR>

" Use <Tab> and <S-Tab> to navigate the completion list
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" }


" status line config
func! GitInfo()
    let l:head = get(g:, 'coc_git_status', '')
    let l:head = l:head != '' ? printf(' %s ', l:head) : ''
    let l:status = get(b:, 'coc_git_status', '')
    let l:status = l:status != '' ? printf('%s ', trim(l:status)) : ''
    return l:head . l:status
endf

" to feel right at home {

set nocompatible
filetype plugin on
syntax on
let g:vimwiki_list = [{'path': '~/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]

"}

" move lines with shift+up/down (yes I use the arrow keys)
nnoremap <S-Down> :m .+1<CR>==
nnoremap <S-Up> :m .-2<CR>==
inoremap <S-Down> <Esc>:m .+1<CR>==gi
inoremap <S-Up> <Esc>:m .-2<CR>==gi
vnoremap <S-Down> :m '>+1<CR>gv=gv
vnoremap <S-Up> :m '<-2<CR>gv=gv
