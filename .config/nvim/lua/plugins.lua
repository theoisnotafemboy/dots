return require('packer').startup(function()
  -- Packer can manage itself, fun, isn't it?
  use 'wbthomason/packer.nvim'

  use 'lukas-reineke/indent-blankline.nvim'
  use 'iamcco/markdown-preview.nvim'

  -- coding things
  use {'neoclide/coc.nvim', branch = 'master', run = 'yarn install --frozen-lockfile'}
  use 'yaocccc/nvim-lines.lua'
  use 'tpope/vim-surround'
  use 'tpope/vim-commentary'

  use 'jiangmiao/auto-pairs'

  -- to feel right at home {
  use 'vimwiki/vimwiki'

  -- }
end)

