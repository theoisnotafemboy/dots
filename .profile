export EDITOR=nvim
export BROWSER=chromium
export TERM=alacritty
export MAIL=thunderbird
export QT_QPA_PLATFORMTHEME="qt5ct"
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
